﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-moment-picker.directive', []);

    angular
    .module('funcef-moment-picker', [
      'funcef-moment-picker.directive'
    ]);
})();