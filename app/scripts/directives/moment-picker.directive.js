﻿(function () {
    'use strict';

    angular
      .module('funcef-moment-picker.directive')
      .directive('ngfMomentPicker', ngfMomentPicker);

    ngfMomentPicker.$inject = ['$timeout'];

    /* @ngInject */
    function ngfMomentPicker($timeout) {
        return {
            replace: false,
            restrict: 'A',
            compile: function (element, attr) {
                if (!element.attr('moment-change')) {
                    element.find('input').attr('ng-model-options', "{updateOn : 'blur'}");
                    $timeout(function () {
                        element.find('input').mask('99/99/9999');
                    }, 500);
                }
            }
        };
    }
})();