# Componente - FUNCEF-MomentPicker

## Descrição

- Componente MomentPicker

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-moment-picker --save.
```

## Script

```html
    <script src="bower_components/funcef-moment-picker/dist/funcef-moment-picker.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
    <link rel="stylesheet" href="bower_components/funcef-moment-picker/dist/funcef-moment-picker.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-moment-picker dentro do módulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-moment-picker']);
```

## Uso

```html
    ????
```

## Desinstalação:

```
uninstall funcef-moment-picker --save